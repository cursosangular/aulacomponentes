import { Component } from '@angular/core';
import { Post } from './core/models/post';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  posts: Array<Post> = [
    {
      id: 1,
      autor: 'Charles França',
      titulo: 'Meu post 1',
      dataPublicacao: '10/10/2010'
    },
    {
      id: 2,
      autor: 'João',
      titulo: 'Meu post 2',
      dataPublicacao: '25/10/2010'
    }
  ]
  title = 'aulaComponents';

  cardClicado(post: Post) {
    alert(post.id);
  }
}
